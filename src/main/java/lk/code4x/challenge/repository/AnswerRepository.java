package lk.code4x.challenge.repository;

import lk.code4x.challenge.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
