package lk.code4x.challenge.repository;

import lk.code4x.challenge.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("FROM Student s WHERE s.username=:username OR s.telephoneNumber=:telephoneNumber OR s.nicNumber=:nicNumber")
    public Student getStudentUniqData(@Param("username") String username,
                                      @Param("telephoneNumber") String telephoneNumber,
                                      @Param("nicNumber") String nicNumber);

    @Query("FROM Student s WHERE s.fullName LIKE %:value% OR s.telephoneNumber LIKE %:value% OR s.school LIKE %:value%")
    public List<Student> findStudents(@Param("value") String value);

    @Query("FROM Student s WHERE s.username=:username")
    public Student getStudentByUsername(@Param("username") String username);
}
