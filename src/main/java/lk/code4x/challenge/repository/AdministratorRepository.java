package lk.code4x.challenge.repository;

import lk.code4x.challenge.model.Administrator;
import lk.code4x.challenge.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {

    @Query("FROM Administrator a WHERE a.username=:username")
    public Administrator getAdministratorByUsername(@Param("username") String username);

    @Modifying
    @Query("DELETE FROM Student s WHERE s.school=:school")
    public void deleteBulkOfStudents(@Param("school") String school);
}
