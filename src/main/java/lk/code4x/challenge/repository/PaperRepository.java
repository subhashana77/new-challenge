package lk.code4x.challenge.repository;

import lk.code4x.challenge.model.Papers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PaperRepository extends JpaRepository<Papers, Long> {
    @Query("FROM Papers p WHERE p.questionTitle LIKE %:value% OR p.question LIKE %:value% OR p.correctAnswer LIKE %:value%")
    public List<Papers> findPapers(@Param("value") String value);
}
