package lk.code4x.challenge.service;

import lk.code4x.challenge.dto.*;
import lk.code4x.challenge.model.Student;

import java.util.HashSet;
import java.util.List;

public interface AdministratorService {
    public ResponseDTO newAdministrator(AdministratorDTO administratorDTO);
    public ResponseDTO getAllAdministrators();
    public ResponseDTO updateAdministrator(AdministratorDTO administratorDTO, Long id);
    public ResponseDTO deleteAdministrator(Long id);
    public ResponseDTO adminLogin(String username, String password);
    public ResponseDTO newPaper(PapersDTO papersDTO);
    public List<StudentDTO> getAllStudents();
    public List<StudentDTO> findStudent(String values);
    public String deleteBulkOfStudents(String school);
    public HashSet<String> getAllSchool();
    public List<PapersDTO> getAllPapers();
    public List<PapersDTO> findPaper(String value);
    public String updatePapers(PapersDTO papersDTO, Long id);
    public String deletePapers(Long id);
}
