package lk.code4x.challenge.service;

import lk.code4x.challenge.dto.AnswerDTO;
import lk.code4x.challenge.dto.StudentDTO;

import java.util.List;

public interface StudentService {
    public String newStudent(StudentDTO studentDTO);
    public StudentDTO loginStudent(String username, String password);
    public String updateStudentData(StudentDTO studentDTO, Long id);
    public String deleteStudent(Long id);
    public String submitAnswers(AnswerDTO answerDTO);
}
