package lk.code4x.challenge.service.impl;

import lk.code4x.challenge.dto.*;
import lk.code4x.challenge.model.Administrator;
import lk.code4x.challenge.model.Papers;
import lk.code4x.challenge.model.Student;
import lk.code4x.challenge.repository.AdministratorRepository;
import lk.code4x.challenge.repository.PaperRepository;
import lk.code4x.challenge.repository.StudentRepository;
import lk.code4x.challenge.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;
    @Autowired
    private PaperRepository paperRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public ResponseDTO newAdministrator(AdministratorDTO administratorDTO) {

        ResponseDTO adminResponseData = new ResponseDTO();

        try {
//            use to save new administrator
            Administrator administrator = new Administrator();
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            administrator.setFullName(administratorDTO.getFullName());
            administrator.setNicNumber(administratorDTO.getNicNumber());
            administrator.setTelephoneNumber(administratorDTO.getTelephoneNumber());
            administrator.setUsername(administratorDTO.getUsername());
            String encodedPassword = passwordEncoder.encode(administratorDTO.getPassword());
            administrator.setPassword(encodedPassword);

            administratorRepository.save(administrator);

//            use to sent data as front end api response
            AdministratorDTO adminResponseDTO = new AdministratorDTO();

            adminResponseDTO.setAdminId(administrator.getAdminId());
            adminResponseDTO.setFullName(administrator.getFullName());
            adminResponseDTO.setNicNumber(administrator.getNicNumber());
            adminResponseDTO.setTelephoneNumber(administrator.getTelephoneNumber());
            adminResponseDTO.setUsername(administrator.getUsername());
            adminResponseDTO.setPassword(administrator.getPassword());

            adminResponseData.setSuccess(true);
            adminResponseData.setMessage("Welcome " + administratorDTO.getFullName());
            adminResponseData.setData(adminResponseDTO);
            return adminResponseData;

        } catch (Exception exception) {

            adminResponseData.setSuccess(false);
            adminResponseData.setMessage("Admin Registration Failure");
            adminResponseData.setData(null);
            return adminResponseData;
        }
    }

    @Override
    public ResponseDTO getAllAdministrators() {
        List<Administrator> allAdmins = administratorRepository.findAll();

        ArrayList<AdministratorDTO> allAdminsDto = new ArrayList<>();

        ResponseDTO adminResponseData = new ResponseDTO();

        try {
            for (Administrator administrator : allAdmins) {

                AdministratorDTO administratorDTO = new AdministratorDTO();

                administratorDTO.setAdminId(administrator.getAdminId());
                administratorDTO.setFullName(administrator.getFullName());
                administratorDTO.setNicNumber(administrator.getNicNumber());
                administratorDTO.setTelephoneNumber(administrator.getTelephoneNumber());
                administratorDTO.setUsername(administrator.getUsername());
                administratorDTO.setPassword(administrator.getPassword());

                allAdminsDto.add(administratorDTO);
            }

            adminResponseData.setSuccess(true);
            adminResponseData.setMessage("Administrators are found");
            adminResponseData.setData(allAdminsDto);
            return adminResponseData;
        } catch (Exception exception) {

            adminResponseData.setSuccess(false);
            adminResponseData.setMessage("Cannot found any administrators");
            adminResponseData.setData(null);
            return adminResponseData;
        }


    }

    @Override
    public ResponseDTO updateAdministrator(AdministratorDTO administratorDTO, Long id) {

        ResponseDTO adminResponseData = new ResponseDTO();

        try {
//            use to save new administrator
            Optional<Administrator> adminById = administratorRepository.findById(id);

            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            Administrator administrator = adminById.get();

            administrator.setFullName(administratorDTO.getFullName());
            administrator.setNicNumber(administratorDTO.getNicNumber());
            administrator.setTelephoneNumber(administratorDTO.getTelephoneNumber());
            administrator.setUsername(administratorDTO.getUsername());
            String encodedPassword = passwordEncoder.encode(administratorDTO.getPassword());
            administrator.setPassword(encodedPassword);

            administratorRepository.save(administrator);

//            use to sent data as front end api response
            AdministratorDTO adminResponseDTO = new AdministratorDTO();

            adminResponseDTO.setAdminId(administrator.getAdminId());
            adminResponseDTO.setFullName(administrator.getFullName());
            adminResponseDTO.setNicNumber(administrator.getNicNumber());
            adminResponseDTO.setTelephoneNumber(administrator.getTelephoneNumber());
            adminResponseDTO.setUsername(administrator.getUsername());
            adminResponseDTO.setPassword(administrator.getPassword());

            adminResponseData.setSuccess(true);
            adminResponseData.setMessage(administratorDTO.getFullName() + " Successfully Updated");
            adminResponseData.setData(adminResponseDTO);
            return adminResponseData;

        } catch (Exception exception) {

            adminResponseData.setSuccess(false);
            adminResponseData.setMessage("Cannot update this administrator");
            adminResponseData.setData(null);
            return adminResponseData;
        }
    }

    @Override
    public ResponseDTO deleteAdministrator(Long id) {

        ResponseDTO adminResponseData = new ResponseDTO();

        Optional<Administrator> findAdminByAdmin = administratorRepository.findById(id);

        if (findAdminByAdmin.isPresent()) {

            Administrator deletedAdmin = findAdminByAdmin.get();
            String adminFullname = deletedAdmin.getFullName();

            administratorRepository.deleteById(id);

            adminResponseData.setSuccess(true);
            adminResponseData.setMessage("Successfully deleted " + adminFullname + "'s data");
            adminResponseData.setData(null);
            return adminResponseData;
        } else {

            adminResponseData.setSuccess(false);
            adminResponseData.setMessage("You cannot delete data");
            adminResponseData.setData(null);
            return adminResponseData;
        }
    }

    @Override
    public ResponseDTO adminLogin(String username, String password) {

        Administrator administratorByUsername = administratorRepository.getAdministratorByUsername(username);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        ResponseDTO adminResponseData = new ResponseDTO();

        if (encoder.matches(password, administratorByUsername.getPassword())) {

            AdministratorDTO administratorDTO = new AdministratorDTO();

            administratorDTO.setFullName(administratorByUsername.getFullName());
            administratorDTO.setNicNumber(administratorByUsername.getNicNumber());
            administratorDTO.setTelephoneNumber(administratorByUsername.getTelephoneNumber());
            administratorDTO.setUsername(administratorByUsername.getUsername());

            adminResponseData.setSuccess(true);
            adminResponseData.setMessage("Welcome " + administratorByUsername.getFullName());
            adminResponseData.setData(administratorDTO);
            return adminResponseData;

        } else {
            adminResponseData.setSuccess(false);
            adminResponseData.setMessage("Sorry. You cannot logging");
            adminResponseData.setData(null);
            return adminResponseData;
        }


    }

    @Override
    public ResponseDTO newPaper(PapersDTO papersDTO) {

        ResponseDTO adminResponseData = new ResponseDTO();

        try {
            Papers papers = new Papers();

            papers.setQuestionTitle(papersDTO.getQuestionTitle());
            papers.setQuestion(papersDTO.getQuestion());
            papers.setAnswerOne(papersDTO.getAnswerOne());
            papers.setAnswerTwo(papersDTO.getAnswerTwo());
            papers.setAnswerThree(papersDTO.getAnswerThree());
            papers.setAnswerFour(papersDTO.getAnswerFour());
            papers.setCorrectAnswer(papersDTO.getCorrectAnswer());

            paperRepository.save(papers);

            PapersDTO adminResponseDTO = new PapersDTO();

            adminResponseDTO.setPaperId(papers.getPaperId());
            adminResponseDTO.setQuestionTitle(papers.getQuestionTitle());
            adminResponseDTO.setQuestion(papers.getQuestion());
            adminResponseDTO.setAnswerOne(papers.getAnswerOne());
            adminResponseDTO.setAnswerTwo(papers.getAnswerTwo());
            adminResponseDTO.setAnswerThree(papers.getAnswerThree());
            adminResponseDTO.setAnswerFour(papers.getAnswerFour());
            adminResponseDTO.setCorrectAnswer(papers.getCorrectAnswer());

            adminResponseData.setSuccess(true);
            adminResponseData.setMessage("successfully added a question to " + papersDTO.getQuestionTitle());
            adminResponseData.setData(adminResponseDTO);
            return adminResponseData;
        } catch (Exception exception) {

            adminResponseData.setSuccess(false);
            adminResponseData.setMessage("Paper added fail");
            adminResponseData.setData(null);
            return adminResponseData;
        }
    }

    @Override
    public List<StudentDTO> getAllStudents() {
        List<Student> allStudents = studentRepository.findAll();

        ArrayList<StudentDTO> studentList = new ArrayList<>();

        for (Student student : allStudents) {
            StudentDTO studentDTO = new StudentDTO();

            studentDTO.setStudentId(student.getStudentId());
            studentDTO.setFullName(student.getFullName());
            studentDTO.setTelephoneNumber(student.getTelephoneNumber());
            studentDTO.setNicNumber(student.getNicNumber());
            studentDTO.setSchool(student.getSchool());
            studentDTO.setGrade(student.getGrade());
            studentDTO.setUsername(student.getUsername());
            studentDTO.setPassword(student.getPassword());

            studentList.add(studentDTO);
        }
        return studentList;
    }

    @Override
    public List<StudentDTO> findStudent(String values) {
        List<Student> students = studentRepository.findStudents(values);

        ArrayList<StudentDTO> studentDTOS = new ArrayList<>();

        if (!students.isEmpty()) {
            for (Student student: students) {
                StudentDTO studentDTO = new StudentDTO();

                studentDTO.setStudentId(student.getStudentId());
                studentDTO.setFullName(student.getFullName());
                studentDTO.setTelephoneNumber(student.getTelephoneNumber());
                studentDTO.setNicNumber(student.getNicNumber());
                studentDTO.setSchool(student.getSchool());
                studentDTO.setGrade(student.getGrade());
                studentDTO.setUsername(student.getUsername());
                studentDTO.setPassword(student.getPassword());

                studentDTOS.add(studentDTO);
            }
            return studentDTOS;
        }
        return null;
    }

    @Override
    @Transactional
    public String deleteBulkOfStudents(String school) {
        administratorRepository.deleteBulkOfStudents(school);
        return "Deleted";
    }

    @Override
    public HashSet<String> getAllSchool() {
        List<Student> allStudentsData = studentRepository.findAll();

        HashSet<String> schoolSet = new HashSet<>();

        for (Student student : allStudentsData) {
            schoolSet.add(student.getSchool());
        }

        return schoolSet;
    }

    @Override
    public List<PapersDTO> getAllPapers() {
        List<Papers> allPapers = paperRepository.findAll();
        ArrayList<PapersDTO> paperList = new ArrayList<>();

        for (Papers papers : allPapers) {
            PapersDTO papersDTO = new PapersDTO();

            papersDTO.setPaperId(papers.getPaperId());
            papersDTO.setQuestionTitle(papers.getQuestionTitle());
            papersDTO.setQuestion(papers.getQuestion());
            papersDTO.setAnswerOne(papers.getAnswerOne());
            papersDTO.setAnswerTwo(papers.getAnswerTwo());
            papersDTO.setAnswerThree(papers.getAnswerThree());
            papersDTO.setAnswerFour(papers.getAnswerFour());
            papersDTO.setCorrectAnswer(papers.getCorrectAnswer());

            paperList.add(papersDTO);
        }

        return paperList;
    }

    @Override
    public List<PapersDTO> findPaper(String value) {
        List<Papers> foundPapers = paperRepository.findPapers(value);

        ArrayList<PapersDTO> paperList = new ArrayList<>();

        if (!foundPapers.isEmpty()) {
            for (Papers papers : foundPapers) {
                PapersDTO papersDTO = new PapersDTO();

                papersDTO.setPaperId(papers.getPaperId());
                papersDTO.setQuestionTitle(papers.getQuestionTitle());
                papersDTO.setQuestion(papers.getQuestion());
                papersDTO.setAnswerOne(papers.getAnswerOne());
                papersDTO.setAnswerTwo(papers.getAnswerTwo());
                papersDTO.setAnswerThree(papers.getAnswerThree());
                papersDTO.setAnswerFour(papers.getAnswerFour());
                papersDTO.setCorrectAnswer(papers.getCorrectAnswer());

                paperList.add(papersDTO);
            }
        }
        return paperList;
    }

    @Override
    public String updatePapers(PapersDTO papersDTO, Long id) {
        Optional<Papers> papersById = paperRepository.findById(id);

        if (papersById.isPresent()) {
            Papers papers = papersById.get();

            papers.setQuestionTitle(papersDTO.getQuestionTitle());
            papers.setQuestion(papersDTO.getQuestion());
            papers.setAnswerOne(papersDTO.getAnswerOne());
            papers.setAnswerTwo(papersDTO.getAnswerTwo());
            papers.setAnswerThree(papersDTO.getAnswerThree());
            papers.setAnswerFour(papersDTO.getAnswerFour());
            papers.setCorrectAnswer(papersDTO.getCorrectAnswer());

            paperRepository.save(papers);
            return "Question Updated";
        } else {
            return "Cannot find Question";
        }
    }

    @Override
    public String deletePapers(Long id) {
        Optional<Papers> paperById = paperRepository.findById(id);

        if (paperById.isPresent()) {

            paperRepository.deleteById(id);
            return "Delete Successfully";

        } else {
            return "Paper cannot found";
        }
    }
}