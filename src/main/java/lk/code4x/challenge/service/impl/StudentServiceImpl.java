package lk.code4x.challenge.service.impl;

import lk.code4x.challenge.dto.AnswerDTO;
import lk.code4x.challenge.dto.StudentDTO;
import lk.code4x.challenge.model.Answer;
import lk.code4x.challenge.model.Papers;
import lk.code4x.challenge.model.Student;
import lk.code4x.challenge.repository.AnswerRepository;
import lk.code4x.challenge.repository.PaperRepository;
import lk.code4x.challenge.repository.StudentRepository;
import lk.code4x.challenge.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private PaperRepository paperRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Override
    public String newStudent(StudentDTO studentDTO) {

        String studentFullName = studentDTO.getFullName();
        String studentGrade = studentDTO.getGrade();
        String studentSchool = studentDTO.getSchool();
        String studentUsername = studentDTO.getUsername();
        String studentTelephone = studentDTO.getTelephoneNumber();
        String studentNic = studentDTO.getNicNumber();
        String studentPassword = studentDTO.getPassword();

        Student studentUniqData = studentRepository.getStudentUniqData(studentUsername, studentTelephone, studentNic);

        if (studentUniqData == null) {
            Student student = new Student();

            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            student.setFullName(studentFullName);
            student.setGrade(studentGrade);
            student.setNicNumber(studentNic);
            student.setSchool(studentSchool);
            student.setTelephoneNumber(studentTelephone);
            student.setUsername(studentUsername);
            student.setPassword(passwordEncoder.encode(studentPassword));

            studentRepository.save(student);

            return "Welcome " + studentFullName;

        } else {
            if (studentUsername.equalsIgnoreCase(studentUniqData.getUsername())) {
                return "Username is already taken";
            } else if (studentTelephone.equalsIgnoreCase(studentUniqData.getTelephoneNumber())) {
                return "Telephone number is already taken";
            } else if (studentNic.equalsIgnoreCase(studentUniqData.getNicNumber())) {
                return "NIC number is already taken";
            } else {
                return "Something went wrong";
            }
        }
    }

    @Override
    public StudentDTO loginStudent(String username, String password) {

        Student studentByUsername = studentRepository.getStudentByUsername(username);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        if (encoder.matches(password, studentByUsername.getPassword())) {

            StudentDTO studentDTO = new StudentDTO();

            studentDTO.setFullName(studentByUsername.getFullName());
            studentDTO.setTelephoneNumber(studentByUsername.getTelephoneNumber());
            studentDTO.setNicNumber(studentByUsername.getNicNumber());
            studentDTO.setSchool(studentByUsername.getSchool());
            studentDTO.setGrade(studentByUsername.getGrade());
            studentDTO.setUsername(studentByUsername.getUsername());

            return studentDTO;
        } else {
            return null;
        }

    }

    @Override
    public String updateStudentData(StudentDTO studentDTO, Long id) {
        Optional<Student> studentById = studentRepository.findById(id);

        if (studentById.isPresent()) {
            try{
                Student student = studentById.get();

                student.setFullName(studentDTO.getFullName());
                student.setTelephoneNumber(studentDTO.getTelephoneNumber());
                student.setNicNumber(studentDTO.getNicNumber());
                student.setSchool(studentDTO.getSchool());
                student.setGrade(studentDTO.getGrade());
                student.setUsername(studentDTO.getUsername());
                student.setPassword(studentDTO.getPassword());

                studentRepository.save(student);

                return studentDTO.getFullName() + "Update Successfully";

            } catch (Exception exception) {
                return "Cannot duplicate NIC or Telephone Number";
            }
        } else {
            return "Student cannot find";
        }
    }

    @Override
    public String deleteStudent(Long id) {
        Optional<Student> studentById = studentRepository.findById(id);
        if (studentById.isPresent()) {
            studentRepository.deleteById(id);
            return "Delete successfully";
        } else {
            return "Student not found to delete";
        }
    }

    @Override
    public String submitAnswers(AnswerDTO answerDTO) {

        try {
            String studentAnswers = answerDTO.getStudentAnswer();

            Long studentId = answerDTO.getStudentId();
            Student studentById = studentRepository.getById(studentId);

            Long paperId = answerDTO.getPaperId();
            Papers paperById = paperRepository.getById(paperId);

            Answer answer = new Answer();

            answer.setStudentAnswer(studentAnswers);
            answer.setStudent(studentById);
            answer.setPapers(paperById);

            answerRepository.save(answer);

            return "Answer Submitted";
        } catch (Exception exception) {

            return "Student or paper assign incorrect";
        }
    }
}
