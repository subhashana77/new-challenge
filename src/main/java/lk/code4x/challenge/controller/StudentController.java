package lk.code4x.challenge.controller;

import lk.code4x.challenge.dto.AnswerDTO;
import lk.code4x.challenge.dto.StudentDTO;
import lk.code4x.challenge.model.Student;
import lk.code4x.challenge.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1.0/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/new")
    public String newStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.newStudent(studentDTO);
    }

    @PostMapping("/login")
    public StudentDTO loginStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.loginStudent(studentDTO.getUsername(), studentDTO.getPassword());
    }

    @PutMapping("/update/{id}")
    public String updateStudent(@RequestBody StudentDTO studentDTO, @PathVariable("id") Long id) {
        return studentService.updateStudentData(studentDTO, id);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteStudent(@PathVariable("id") Long id) {
        return studentService.deleteStudent(id);
    }

    @PostMapping("/submit-answers")
    public String submitAnswers(@RequestBody AnswerDTO answerDTO) {
        return studentService.submitAnswers(answerDTO);
    }
}
