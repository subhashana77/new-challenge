package lk.code4x.challenge.controller;

import lk.code4x.challenge.dto.*;
import lk.code4x.challenge.model.Student;
import lk.code4x.challenge.service.AdministratorService;
import lk.code4x.challenge.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping("/api/v1.0/admin")
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private StudentService studentService;

    @PostMapping("/new")
    public ResponseDTO newAdministrator(@RequestBody AdministratorDTO administratorDTO ) {
        return administratorService.newAdministrator(administratorDTO);
    }

    @GetMapping("/get")
    public ResponseDTO getAllAdministrators() {
        return administratorService.getAllAdministrators();
    }

    @PutMapping("/update/{id}")
    public ResponseDTO updateAdministrator(@RequestBody AdministratorDTO administratorDTO, @PathVariable("id") Long id) {
        return administratorService.updateAdministrator(administratorDTO, id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseDTO deleteAdministrator(@PathVariable("id") Long id) {
        return administratorService.deleteAdministrator(id);
    }

    @PostMapping("/login")
    public ResponseDTO loginAdmin(@RequestBody AdministratorDTO administratorDTO) {
        return administratorService.adminLogin(administratorDTO.getUsername(), administratorDTO.getPassword());
    }

    @PostMapping("/new-paper")
    public ResponseDTO newPaper(@RequestBody PapersDTO papersDTO) {
        return administratorService.newPaper(papersDTO);
    }

    @PutMapping("/update-student/{id}")
    public String updateStudentByAdmin(@RequestBody StudentDTO studentDTO, @PathVariable("id") Long id) {
        return studentService.updateStudentData(studentDTO, id);
    }

    @GetMapping("/getAllStudent")
    public List<StudentDTO> getAllStudents() {
        return administratorService.getAllStudents();
    }

    @GetMapping("/find-student")
    public List<StudentDTO> findStudent(@RequestParam("values") String values) {
        return administratorService.findStudent(values);
    }

    @DeleteMapping("/delete-student/{id}")
    public String deleteStudent(@PathVariable("id") Long id) {
        return studentService.deleteStudent(id);
    }

    @DeleteMapping("/delete-bulk-student")
    public String deleteBulkOfStudents(@RequestParam("school") String school) {
        return administratorService.deleteBulkOfStudents(school);
    }

    @GetMapping("/get-schools")
    public HashSet<String> getAllSchools() {
        return administratorService.getAllSchool();
    }

    @GetMapping("/get-all-papers")
    public List<PapersDTO> getAllPapers() {
        return administratorService.getAllPapers();
    }

    @GetMapping("/find-paper")
    public List<PapersDTO> findPaper(@RequestParam("value") String value) {
        return administratorService.findPaper(value);
    }

    @PutMapping("/edit-papers/{id}")
    public String editPapers(@RequestBody PapersDTO papersDTO, @PathVariable("id") Long id) {
        return administratorService.updatePapers(papersDTO, id);
    }

    @DeleteMapping("/delete-paper/{id}")
    public String deletePapers(@PathVariable("id") Long id) {
        return administratorService.deletePapers(id);
    }
}
