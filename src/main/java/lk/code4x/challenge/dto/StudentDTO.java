package lk.code4x.challenge.dto;

public class StudentDTO {
    private Long studentId;
    private String fullName;
    private String nicNumber;
    private String telephoneNumber;
    private String school;
    private String grade;
    private String username;
    private String password;

    public StudentDTO() {
    }

    public StudentDTO(Long studentId, String fullName, String nicNumber, String telephoneNumber, String school, String grade, String username, String password) {
        this.studentId = studentId;
        this.fullName = fullName;
        this.nicNumber = nicNumber;
        this.telephoneNumber = telephoneNumber;
        this.school = school;
        this.grade = grade;
        this.username = username;
        this.password = password;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNicNumber() {
        return nicNumber;
    }

    public void setNicNumber(String nicNumber) {
        this.nicNumber = nicNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
