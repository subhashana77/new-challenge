package lk.code4x.challenge.dto;

public class AdministratorDTO {
    private Long adminId;
    private String fullName;
    private String nicNumber;
    private String telephoneNumber;
    private String username;
    private String password;

    public AdministratorDTO() {
    }

    public AdministratorDTO(String fullName, String nicNumber, String telephoneNumber, String username) {
        this.fullName = fullName;
        this.nicNumber = nicNumber;
        this.telephoneNumber = telephoneNumber;
        this.username = username;
    }

    public AdministratorDTO(Long adminId, String fullName, String nicNumber, String telephoneNumber, String username, String password) {
        this.adminId = adminId;
        this.fullName = fullName;
        this.nicNumber = nicNumber;
        this.telephoneNumber = telephoneNumber;
        this.username = username;
        this.password = password;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNicNumber() {
        return nicNumber;
    }

    public void setNicNumber(String nicNumber) {
        this.nicNumber = nicNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
