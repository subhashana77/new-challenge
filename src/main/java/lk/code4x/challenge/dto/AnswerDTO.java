package lk.code4x.challenge.dto;

public class AnswerDTO {
    private Long answerId;
    private String studentAnswer;
    private Long studentId;
    private Long paperId;

    public AnswerDTO() {
    }

    public AnswerDTO(Long answerId, String studentAnswer, Long studentId, Long paperId) {
        this.answerId = answerId;
        this.studentAnswer = studentAnswer;
        this.studentId = studentId;
        this.paperId = paperId;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getPaperId() {
        return paperId;
    }

    public void setPaperId(Long paperId) {
        this.paperId = paperId;
    }
}
